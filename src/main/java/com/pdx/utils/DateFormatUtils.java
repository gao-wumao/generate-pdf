package com.pdx.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-18 23:42
 * @Description 时间格式化工具类
 */
public class DateFormatUtils {

    public static String dateFormatToMonth(Date date){
        SimpleDateFormat sbf = new SimpleDateFormat("yyyy-MM");
        String format = sbf.format(date);
        return format;
    }

    public static String dateRotation(){
        Date date = new Date();
        System.out.println(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(date).toString();
        String newDate = format.substring(0,4)+format.substring(5,7)+format.substring(8);
        return newDate;
    }
}
