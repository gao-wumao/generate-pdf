package com.pdx.utils;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-18 23:42
 * @Description Aspose 工具类
 */
public class AsposeWordsUtils {
    /**
     * Word转PDF操作
     *
     * @param doc        源文件
     * @param targetFile 目标文件
     */
    public static void doc2pdf(Document doc, String targetFile) {
        try {
            long old = System.currentTimeMillis();
            //新建一个空白pdf文档
            File file = new File(targetFile);
            FileOutputStream os = new FileOutputStream(file);
            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(os, SaveFormat.PDF);
            os.close();
            long now = System.currentTimeMillis();
            //转化用时
            System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        InputStream is = Document.class
                .getResourceAsStream("/com.aspose.words.lic_2999.xml");
        License asposeLicense = new License();
        asposeLicense.setLicense(is);
        System.out.println("Aspose isLicensed: " + asposeLicense.isLicensed());

    }
}
