package com.pdx.vo;

import lombok.Data;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-18 23:42
 * @Description
 */
@Data
public class PDFVo {

    /*
    * 姓名
    * */
    private String username;
    /*
    * 院校
    * */
    private String school;
    /*
    * 年龄
    * */
    private Integer age;
    /*
    * 电话
    * */
    private String phone;
    /*
    * 邮箱
    * */
    private String email;
    /*
    * 性别
    * */
    private String sex;
    /*
    * 民族
    * */
    private String nation;
    /*
    * 学历
    * */
    private String education;

    /*
    * 专业
    * */
    private String major;

}
