package com.pdx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class GeneratorPDFApplication
{
    public static void main( String[] args ) {
        SpringApplication.run(GeneratorPDFApplication.class,args);
    }
}
