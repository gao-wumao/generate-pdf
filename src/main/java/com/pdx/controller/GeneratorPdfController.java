package com.pdx.controller;

import com.alibaba.fastjson.JSON;
import com.pdx.service.PdfService;
import com.pdx.vo.PDFVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-18 23:28
 * @Description
 */
@RestController
@Api(tags = "Java生成PDF")
public class GeneratorPdfController {

    @Autowired
    private PdfService pdfService;


    @PostMapping("/generatorPdf")
    @ApiOperation(value = "Java生成PDF")
    public String generatorPdf(HttpServletRequest request, @RequestBody PDFVo vo) throws Exception {
        pdfService.generatorPdf(request,vo);
        return JSON.toJSONString("ok") ;
    }


}
