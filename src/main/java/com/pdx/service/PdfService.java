package com.pdx.service;

import com.aspose.words.Document;
import com.aspose.words.FindReplaceOptions;
import com.aspose.words.ImportFormatMode;
import com.aspose.words.Range;
import com.pdx.utils.AsposeWordsUtils;
import com.pdx.utils.DateFormatUtils;
import com.pdx.vo.PDFVo;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * @author PDX
 * @website https://blog.csdn.net/Gaowumao
 * @Date 2022-05-18 23:42
 * @Description
 */
@Service
public class PdfService {

    public void generatorPdf(HttpServletRequest request, PDFVo vo) throws Exception {
        File directory = new File("");
        String canonicalPath = directory.getCanonicalPath();
        String path = canonicalPath+"/src/main/resources/templates/single.docx";
        Document firstDocument = null;
        String start = "2019-9-1";
        String end = "2023-6-30";
        Document document = new Document(path);
        Range range = document.getRange();
        range.replace("username",vo.getUsername(),new FindReplaceOptions());
        range.replace("school",vo.getSchool(),new FindReplaceOptions());
        range.replace("age",String.valueOf(vo.getAge()),new FindReplaceOptions());
        range.replace("phone",vo.getPhone(),new FindReplaceOptions());
        range.replace("email",vo.getEmail(),new FindReplaceOptions());
        range.replace("sex",vo.getSex(),new FindReplaceOptions());
        range.replace("nation",vo.getNation(),new FindReplaceOptions());
        range.replace("education",vo.getEducation(),new FindReplaceOptions());
        range.replace("major",vo.getMajor(),new FindReplaceOptions());
        range.replace("start",start,new FindReplaceOptions());
        range.replace("end",end,new FindReplaceOptions());
        if (firstDocument == null){
            firstDocument = document;
        }else {
            //添加文档
            firstDocument.appendDocument(document, ImportFormatMode.KEEP_DIFFERENT_STYLES);
        }
        //生成路径
        String url = System.getProperty("user.dir")+"/upload/";
        isFolderExists(url);
        String date = DateFormatUtils.dateRotation();
        AsposeWordsUtils.doc2pdf(firstDocument,url+date+".pdf");
    }


    /**
     * 验证文件夹是否存在
     * @param strFolder
     * @return
     */
    public static boolean isFolderExists(String strFolder){
        File file = new File(strFolder);
        if (!file.exists())
        {
            if (file.mkdir())
            {
                return true;
            }
            else{
                return false;
            }

        }
        System.out.println("-----------------文件上传路径："+strFolder);
        return true;
    }


    public static void main(String[] args) {
        File directory = new File("");
        try {
            String canonicalPath = directory.getCanonicalPath();
            String path = canonicalPath+"/src/main/resources/templates/single.docx";
            String property = System.getProperty("user.dir")+"/upload";
            System.out.println(property);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
