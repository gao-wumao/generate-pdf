#  :tw-1f33d: 生成PDF案例 :tw-1f33d: 

####  :tw-1f347: 前提介绍
最近也是在非常紧急的准备暑期实习的面试，一边学习一边修补简历，因为之前看到某位大佬的帖子说建议投递的简历形式为PDF，这一下可是把我难死了，索性就可以在网上找寻各种方法，逛了一圈回来发现，网上特别多的这种帮助制作简历的平台，但是都没有把word文档转为PDF的，所以我必须重新在平台上重新输入一遍自己的信息然后再由平台进行制作，但是问题又出现了，部分信息我并不想填，但是不填平台不允许进行下一项的填写，很是令人头疼。经过这么一倒腾，就想着能不能利用自己学的技术做一个简历制作并且还是最终还可以PDF的形式输出呢？

### :tw-1f34d: 技术栈
-  :tw-1f352: SpringBoot
-  :tw-1f353: Swagger
-  :tw-1f351: Aspose
<br>[Aspose的Word转PDF文档地址](https://docs.aspose.com/words/java/convert-a-document-to-pdf/)

###  :tw-1f349: 最终演示
![输入图片说明](bc95n-umied.gif)

###  :tw-1f364: 项目需求分析
仿照超级简历模式进行简历制作，用户只需要填写自己想要填充的信息即可生成属于自己的PDF版简历，不需要再先使用Word填充随后再更改为PDF格式，减少了冗余操作，而且并不会限制用户输入某一部分信息后才能输入下一项信息的需求。


### ❗ ❗注意 ❗ ❗
在创建项目时需要注意版本控制，如果太高或太低就会出现一些恶心的报错信息，下面列举几个我遇到的 ❌

 **No.1** 
```shell
Caused by: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'httpPutFormContentFilter' defined in class path resource [org/springframework/boot/autoconfigure/web/WebMvcAutoConfiguration.class]: Bean instantiation via factory method failed; nested exception is org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.boot.web.filter.OrderedHttpPutFormContentFilter]: Factory method 'httpPutFormContentFilter' threw exception; nested exception is java.lang.NoClassDefFoundError: Could not initialize class com.fasterxml.jackson.databind.ObjectMapper

```
 **No.2** 

```shell
Caused by: java.lang.ClassNotFoundException: com.fasterxml.jackson.databind.ser.std.ToStringSerializerBase
at java.net.URLClassLoader.findClass(URLClassLoader.java:382) ~[?:1.8.0_202]
at java.lang.ClassLoader.loadClass(ClassLoader.java:424) ~[?:1.8.0_202]
at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:349) ~[?:1.8.0_202]
at java.lang.ClassLoader.loadClass(ClassLoader.java:357) ~[?:1.8.0_202]
at java.lang.ClassLoader.defineClass1(Native Method) ~[?:1.8.0_202]
at java.lang.ClassLoader.defineClass(ClassLoader.java:763) ~[?:1.8.0_202]

```
解决方案：

首先将SpringBoot版本降或升到2.3.2.RELEASE

然后直接将下面的依赖导入即可！

```xml
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-annotations</artifactId>
  <version>2.10.1</version>
</dependency>
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-core</artifactId>
  <version>2.10.1</version>
</dependency>
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-databind</artifactId>
  <version>2.10.1</version>
  <exclusions>
    <exclusion>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-annotations</artifactId>
    </exclusion>
  </exclusions>
</dependency>

```

###  :tw-1f31f: 最终结果


在测试项目前需要准备一个`docx`格式的模板<br>
![输入图片说明](image.png)

转换之后：<br>
![输入图片说明](image.png)